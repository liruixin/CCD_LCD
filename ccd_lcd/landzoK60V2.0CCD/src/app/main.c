/******************** (C) COPYRIGHT 2011 蓝宙电子工作室 ********************
 * 文件名       ：main.c
 * 描述         ：工程模版实验
 *               CCD的PIN定义   
ADC1_SE6b -- PTC10      CLK_ClrVal() -- PTE5_OUT = 0      SI_SetVal() -- PTE4_OUT = 1
                 NOKIA5110的PIN定义
LCD_RST = PTB9_OUT       LCD_CE=  PTB7_OUT         LCD_DC = PTB5_OUT
SDIN =  PTB3_OUT        SCLK= PTB1_OUT

**********************************************************************************/
#include "include.h"
#include "calculation.h"
/*************************
设置系统的全局变量
*************************/
extern u8 TIME0flag_5ms   ;
extern u8 TIME0flag_10ms  ;
extern u8 TIME0flag_15ms  ;
extern u8 TIME0flag_20ms  ;
extern u8 TIME1flag_20ms ;
extern u8 TIME1flag_1ms ;
extern u8 TimerFlag20ms;
extern u8 IntegrationTime;
u16 time=0;
u8 AtemP ;
u8 Pixel[128];

void main()       //     ******  主函数开始  ******
{
  u8 Flag=1;           //Flag置0是LCD模块，置1是CCD模块,3是其他函数
/******************************************************************************
                           线性CCD模块
******************************************************************************/
  if(Flag==1){
    volatile u8 i;
   u8 send_data_cnt = 0;
   u8 *pixel_pt;
   u8 bai,shi,ge;
  DisableInterrupts;                             //禁止总中断 
   
  /*********************************************************
  初始化程序
  *********************************************************/
   //自行添加代码
 
   uart_init (UART0 , 9600);                      //初始化UART0，输出脚PTA15，输入脚PTA14，串口频率 9600

  //  adc_init(ADC1, AD6a) ;
    
   gpio_init (PORTA , 17, GPO,HIGH); 
   gpio_init (PORTB , 17, GPO,LOW);   
   pit_init_ms(PIT0, 5);                                    //初始化PIT0，定时时间为： 5ms
   pit_init(PIT1, 10000);                                   //初始化PIT1，定时时间为： 0.2ms 
   
   CCD_init1() ;             //CCD传感器初始化
   LCD_init();
    
  pixel_pt = Pixel;
  for(i=0; i<128+10; i++) {
    *pixel_pt++ = 0;
  }
  

   EnableInterrupts;			                    //开总中断  
   
   /******************************************
    执行程序
    ******************************************/
    while(1)
    {
      if(TIME1flag_1ms == 1)
      {
        TIME1flag_1ms = 0 ;
        
      }     
      
      if(TIME1flag_20ms == 1)
      {
        TIME1flag_20ms = 0 ; 
        /* Sampling CCD data */
        ImageCapture(Pixel);
        /* Calculate Integration Time */
        CalculateIntegrationTime();
        /* Send data to CCDView every 100ms */
        if(++send_data_cnt >= 50) {
          send_data_cnt = 0;
         // SendImageData(Pixel);
          LCD_set_XY(36,3);
          LCD_write_num(IntegrationTime);
         // time+=100;
         // if(time==1000)time=0;
         bai=IntegrationTime/100+48;
         shi=IntegrationTime%100/10+48;
         ge=IntegrationTime%10+48;
         uart_putchar(UART0,bai);
         uart_putchar(UART0,shi);
         uart_putchar(UART0,ge);
         uart_putchar(UART0,'\n');
         
        }
        
        /**********
        计算黑线位置
        **********/
    
      } 

    }
  }//end if
/******************************************************************************
                       NOKIA5110模块 
  
  (最多显示六行，汉字会占两行)（一共有84列，一个汉字宽12，一个字符宽6）（一行最多七个汉字，14个字符）
*******************************************************************************/  
  if(Flag==0){ 
   u16 time=6535; 
   LCD_init();
   LCD_write_chinese_string(0,0,12,7,0,0);
   LCD_write_english_string(0,3,"time=");  
  while(1)
  {  
    LCD_set_XY(30,3);
    LCD_write_num(time);
    time+=100;
    
      time_delay_ms(50);
  }
  }
/*****************************************************************************
                         自定义模块
*****************************************************************************/ 
  if(Flag==3)
  {
    
    } 
 }

  
