/*
 * File:		nokia.h
 * Purpose:		Serial Input/Output routines
 *
 */

#ifndef _NOKIA_H
#define _NOKIA_H

#include "gpio.h"
/********************************************************************/
/*-----------------------------------------------------------------------
LCD_init          : 5110LCD初始化

编写日期          ：2012-11-01
最后修改日期      ：2012-11-01
-----------------------------------------------------------------------*/
#define LCD_RST   PTB9_OUT 
#define LCD_CE    PTB7_OUT
#define LCD_DC    PTB5_OUT
#define SDIN      PTB3_OUT
#define SCLK      PTB1_OUT


void LCD_write_num(u8 num);
void delay_1us(void)  ;
void delay_1ns(void)  ;
void LCD_clear(void);
void LCD_write_byte(u8 dat, u8 command);
void LCD_init(void) ;
void LCD_write_char(u8 c) ;
void LCD_set_XY(u8 X, u8 Y);
void LCD_write_english_string(u8 X,u8 Y,s8 *s);
void LCD_write_chinese_string(u8 X, u8 Y, u8 ch_width,u8 num,u8 line,u8 row);



    

/********************************************************************/

#endif
